

class student:

    x = 10
    @classmethod
    def data(cls):
        cls.x+=1


s1 = student()
s2 = student()

print("x in s1",s1.x)
print("x in s2",s2.x)

s1.data()
print("x in s1",s1.x)
print("x in s2",s2.x)